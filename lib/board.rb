require 'byebug'

class Board

  attr_reader :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    a = pos.first
    b = pos.last
    raise "position already filled" if grid[a][b] != nil
    grid[a][b] = mark
  end

  def empty?(arr)
    a = arr[0]
    b = arr[1]
    grid[a][b] == nil ? true : false
  end

  def winner
    grid.each do |arr|
      return :X if arr.all? { |el| el == :X }
      return :O if arr.all? { |el| el == :O }
    end
    return :X if grid.all? { |arr| arr.first == :X } ||
                 grid.all? { |arr| arr.last == :X }
    return :O if grid.all? { |arr| arr.first == :O } ||
                 grid.all? { |arr| arr.last == :O }
    return :X if (grid[0][0] == :X && grid[1][1] == :X &&
                 grid[2][2] == :X) || (grid[0][2] == :X &&
                 grid[1][1] == :X && grid[2][0] == :X)
    return :O if (grid[0][0] == :O && grid[1][1] == :O &&
                 grid[2][2] == :O) || (grid[0][2] == :O &&
                 grid[1][1] == :O && grid[2][0] == :O)
  end

  def over?
    return true if winner != nil
    grid.each do |arr|
      arr.each do |el|
        return false if el == nil
      end
    end
    true
  end

end
