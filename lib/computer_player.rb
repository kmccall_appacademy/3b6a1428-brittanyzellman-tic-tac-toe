class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    arr_row = check_rows(board)
    arr_column = check_columns(board)
    return arr_row if !arr_row.nil?
    return arr_column if !arr_column.nil?

    a = rand(3)
    b = rand(3)
    while !board.empty?([a, b])
      a = rand(3)
      b = rand(3)
    end
    [a, b]

  end

  private

  def check_rows(board)
    # check each row array to see if there is a winning move and sends
    # it to move_row() to return the position
    board.grid.each_with_index do |arr, i|
      if @mark==:X && arr.count(nil) == 1 && arr.uniq.compact == [:X]
        move_row(board[i])
      elsif @mark==:O && arr.count(nil) == 1 && arr.uniq.compact == [:O]
        move_row(board[i])
      end
    end
    nil
  end

  def check_columns(board)
    # shovels into an array the different columns and then checks those
    # arrays to see if there is a winning move and sends it to
    # move_column() to return the position
    i = 0
    while i < 3
      column = []
      board.grid.each do |arr|
        column << arr[i]
        if @mark == :X && column.length == 3
          return move_column(column, i) if
            column.count(nil) == 1 && column.uniq.compact == [:X]
        elsif @mark == :O && column.length == 3
          return move_column(column, i) if
            column.count(nil) == 1 && column.uniq.compact == [:O]
        end
      end
      i += 1
    end
  end

  def move_row(arr_from_board)
    row = board.grid.index(arr_from_board)
    column = arr_from_board.index(nil)
    [row, column]
  end

  def move_column(arr_from_board, i)
    row = arr_from_board.index(nil)
    [row, i]
  end

end
