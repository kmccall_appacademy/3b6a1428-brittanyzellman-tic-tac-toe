require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require 'byebug'

class Game

  attr_reader :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = @player_one
  end

  def play_turn
    @player_one.display(board)
    @player_two.display(board)
    # debugger
    # mover = current_player
    @board.place_mark(@current_player.get_move, @current_player.mark)
    @current_player = switch_players!
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end

  end

end
