require 'byebug'

class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "where"
    move = gets.chomp
    move.split(",").map(&:to_i)
  end

  def display(board)
    # debugger
    board.grid.each do |arr|
      arr.each do |el|
        puts el
      end
    end
  end

end
